<!doctype html>
<html>
    <head>
        <title>{{NOMBRE_SISTEMA}}</title>
        <meta charset="UTF-8" />
        <meta http-equiv="content-type" content="text/html" />
        <meta http-equiv="Cache-Control" content="no-store" />
        <link rel="stylesheet" href="{{ URL }}public/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ URL }}public/css/default.css">
        <script src="{{ URL }}public/js/jquery-2.2.0.min.js"></script>
        <script src="{{ URL }}public/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{ URL }}public/js/custom.js"></script>
    </head>
    <body>
        <?php Session::init(); ?>
        <div class="container-fluid" id="content">
