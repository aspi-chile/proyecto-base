            {% if error is defined %}
            El error esta definido <br>
            {% endif %}
            <nav class="navbar navbar-default fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1" aria-expanded="false">
                            <span class="sr-only">Menú</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">{{NOMBRE_SISTEMA}}</a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar1">
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <!-- manejo de errores y mensajes -->
            <div class="row">
                <div class="col-md-12">
                    <div id="panelError" class="panel panel-danger" {% if error is not defined %}style="display: none"{% endif %}>
                        <div class="panel-heading"><h3 class="panel-title">Error: </h3>
                            {% if error is defined %}
                                <small><h5>{{ error.mensaje }}</h5></small>
                            {% endif %}
                        </div>
                        <div class="panel-body">
                            {% if error is defined %}
                                {% for llave, errorDescripcion in error.descripcion %}
                                        <b> {{llave}} :</b> <br>
                                    {% for errorDisplay in errorDescripcion %}
                                        &nbsp&nbsp&nbsp {{ errorDisplay }}<br>
                                    {% endfor %}
                                {% endfor %}
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="panelMensaje" class="panel panel-info" style="display: none;">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                        </div>
                        <div class="panel-body"></div>
                    </div>
                </div>
            </div>