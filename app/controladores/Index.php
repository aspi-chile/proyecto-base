<?php
class Index extends Controlador
  {
    function __construct($logger, $vista = null)
      {
        parent::__construct($logger, $vista);
      }
    function index()
      {
        $this->vista->renderTwig('index');
      }
  }
?>
