function manejarRespuesta(data){
    var panelRespuesta
    if(data.error.codigo == 0){
        $('#panelError').hide()
        panelRespuesta = $('#panelMensaje')
        panelRespuestaTitulo = $('#panelMensaje .panel-heading .panel-title')
        panelRespuestaCuerpo = $('#panelMensaje .panel-body')
    }else{
        $('#panelMensaje').hide()
        panelRespuesta = $('#panelError')
        panelRespuestaTitulo = $('#panelError .panel-heading .panel-title')
        panelRespuestaCuerpo = $('#panelError .panel-body')
    }
    panelRespuestaTitulo.text(data.error.mensaje)
    panelRespuesta.show();
}
